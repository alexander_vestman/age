#ifndef AGE_LOGGER_HPP
#define AGE_LOGGER_HPP

#include <iostream>

namespace AGE
{	
	std::ostream& Logger = std::cout;
	
	struct LogEntry
	{	
		std::string m_logMessage;
		enum LogType 
		{
			TYPE_DEBUG,
			TYPE_NOTIFICATION,
			TYPE_WARNING,			
			TYPE_ERROR,
			TYPE_CRITICAL
		}m_logType;
		LogEntry(std::string logMessage, LogType logType = LogEntry::LogType::TYPE_CRITICAL):m_logMessage(logMessage), m_logType(logType){}	
	};

	std::string LogTypeToString(LogEntry::LogType logType)
	{
		switch(logType)
		{
			case LogEntry::LogType::TYPE_DEBUG:
				return "Debug";
			case LogEntry::LogType::TYPE_NOTIFICATION:
				return "Notification";
			case LogEntry::LogType::TYPE_WARNING:
				return "Warning";
			case LogEntry::LogType::TYPE_ERROR:
				return "Error";
			case LogEntry::LogType::TYPE_CRITICAL:
				return "Critical";
		}
	}

	std::ostream& operator<<(std::ostream& ost, const LogEntry& logEntry)
	{
		return ost << LogTypeToString(logEntry.m_logType).c_str() <<": " << logEntry.m_logMessage.c_str() << std::endl;
	}
}




#endif