#ifndef AGE_H
#define AGE_H

#define main AGE_main

extern "C" extern int AGE_main(int argc, char** argv);

#include <AGE\AGE_Initialize.h>
#include <AGE\AGE_Window\AGE_Window.hpp>
#include <AGE\AGE_Window\Input\AGE_Keyboard.hpp>
#include <AGE\AGE_Window\Input\AGE_Mouse.hpp>


#endif