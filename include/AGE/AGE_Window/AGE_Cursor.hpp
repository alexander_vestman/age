#ifndef AGE_CURSOR_HPP
#define AGE_CURSOR_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>

namespace AGE
{
	/// <summary>
	/// A Cursor abstraction that creates a platform specfic cursor handle from pixel data.
	/// If the Cursor object is deallocated or goes out of scope while being used by a window the behaviour is undefined.
	/// </summary>
	class AGE_DLL Cursor
	{
	public:
		/// <summary>
		/// Creates a cursor from the specified parameters.
		/// </summary>
		/// <param name="cursorPixelData"> A pointer to data containing 32-bit RGBA formatted pixel data. The data must contain Width times Height RGBA tupples. </param>
		/// <param name="width"> The Width of the cursor. </param>
		/// <param name="height"> The Height of the cursor. </param>
		/// <param name="hotSpotX"> The horizontal distance relative to the left side of the cursor that the cursor is offset with. </param>
		/// <param name="width"> The vertical distance relative to the top of the cursor that the cursor is offset with. </param>
		Cursor(const unsigned char* cursorPixelData, const int width, const int height, const int hotSpotX, const int hotSpotY);
		~Cursor();

		/// <summary>
		/// Retrieves the platform specific CursorHandle 
		/// </summary>
		inline AGE::IconHandle getCursorHandle() const
		{
			return m_cursorHandle;
		}

	private:
		AGE::CursorHandle m_cursorHandle;
	};

}

#endif