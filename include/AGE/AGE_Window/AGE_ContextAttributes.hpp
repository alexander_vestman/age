#ifndef AGE_CONTEXT_ATTRIBUTES_HPP
#define AGE_CONTEXT_ATTRIBUTES_HPP

namespace AGE
{
	enum class ContextProfile
	{
		CORE_PROFILE,
		COMPATIBILITY_PROFILE
	};

	enum ContextFlags
	{
		NO_FLAGS = 0x0000,
		DEBUG_FLAG = 0x0001,
		FORWARD_FLAG = 0x0002,
		DEBUG_FORWARD_FLAG = DEBUG_FLAG | FORWARD_FLAG
	};

	struct ContextAttributes
	{
		unsigned int m_majorVersion;
		unsigned int m_minorVersion;
		unsigned int m_antiAliasing;
		unsigned int m_colorBitsPerPixel;
		unsigned int m_depthBitsPerPixel;
		unsigned int m_stencilBitsPerPixel;
		AGE::ContextProfile m_contextProfile;
		AGE::ContextFlags m_contextFlags;
	};
}

#endif