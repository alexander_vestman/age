#ifndef AGE_CHAR_EVENT_HPP
#define AGE_CHAR_EVENT_HPP

namespace AGE
{
	/// <summary>
	///	A struct related to a CHARACTER event that is storing the actually typed value of a KeyEvent as a UTF-8 formated character.
	/// One CharEvent may not contain a whole UTF-8 character, in the case that a UTF-8 character is longer than one byte, another CharEvent will be posted right after.
	/// </summary>
	struct CharEvent
	{
		/// <summary>
		///	A non-null terminated UTF-8 formated character. 
		/// </summary>
		char m_character;
	};
}

#endif