#ifndef AGE_MOUSE_INPUT_EVENT_HPP
#define AGE_MOUSE_INPUT_EVENT_HPP

namespace AGE
{
	/// <summary> 
	/// An enumeration of the various mouse buttons. Used for checking states of buttons with the MouseState class and in mouse related events.
	/// </summary>
	enum class Button : unsigned char
	{
		LEFT,
		MIDDLE, 
		RIGHT,
		X_BUTTON_ONE,
		X_BUTTON_TWO
	};

	/// <summary> 
	/// An enumeration that contains the different states of a mouse button
	/// </summary>
	enum class ButtonState : bool
	{
		BUTTON_UP,
		BUTTON_DOWN,
		BUTTON_DOUBLE_CLICK
	};

	/// <summary> 
	/// A structure that contains information related to a MOUSE_BUTTON event.
	/// </summary>
	struct MouseInputEvent
	{
		/// <summary> 
		/// The horizontal position relative to the top-left corner of the client space where the cursor was when the button changed state.
		/// </summary>
		short m_x;

		/// <summary> 
		/// The vertical position relative to the top-left corner of the client space where the cursor was when the button changed state.
		/// </summary>
		short m_y;

		/// <summary> 
		/// The mouse button which changed state.
		/// </summary>
		AGE::Button m_button;

		/// <summary> 
		/// The new state of the mouse button.
		/// </summary>
		AGE::ButtonState m_state;
	};
}

#endif