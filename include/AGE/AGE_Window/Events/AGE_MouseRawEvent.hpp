#ifndef AGE_MOUSE_RAW_EVENT_HPP
#define AGE_MOUSE_RAW_EVENT_HPP

namespace AGE
{
	/// <summary> 
	/// A struct containing information related to MOUSE_RAW events.
	/// </summary>
	struct MouseRawEvent
	{	
		/// <summary> 
		/// The registered horizontal change of the raw input from the mouse.
		/// </summary>
		long m_x;

		/// <summary> 
		/// The registered vertical change of the raw input from the mouse.
		/// </summary>
		long m_y;
	};
}

#endif