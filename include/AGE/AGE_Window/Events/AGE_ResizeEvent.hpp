#ifndef AGE_RESIZE_EVENT_HPP
#define AGE_RESIZE_EVENT_HPP

namespace AGE
{
	/// <summary> 
	/// An enumeration containing the different kinds of resize types. Either maximized, minimized or resized.
	/// </summary>
	enum class ResizeType
	{
		MAXIMIZED,
		MINIMIZED,
		RESIZED
	};

	/// <summary> 
	/// A structure that contains information related to a RESIZE event.
	/// </summary>
	struct ResizeEvent
	{	
		/// <summary> 
		/// Stores which type of resize occured.
		/// </summary>
		ResizeType m_resizeType; 

		/// <summary> 
		/// Contains the width of the new client area of the window in pixels.
		/// </summary>
		unsigned short m_width;

		/// <summary> 
		/// Contains the height of the new client area of the window in pixels.
		/// </summary>
		unsigned short m_height;	
	};
}

#endif