#ifndef AGE_FOCUS_EVENT_HPP
#define AGE_FOCUS_EVENT_HPP

namespace AGE
{
	/// <summary> 
	/// An enumeration containing the different kinds of focus types. Either gain or lose.
	/// </summary>
	enum class FocusType
	{
		GAIN,
		LOSE
	};

	/// <summary> 
	/// A structure that contains information related to a FOCUS event.
	/// </summary>
	struct FocusEvent
	{
		/// <summary> 
		/// Stores if the window lost or gained focus.
		/// </summary>
		FocusType m_focusType;
	};
}

#endif