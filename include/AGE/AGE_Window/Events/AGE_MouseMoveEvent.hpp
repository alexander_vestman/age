#ifndef AGE_MOUSE_MOVE_EVENT_HPP
#define AGE_MOUSE_MOVE_EVENT_HPP

namespace AGE
{	
	/// <summary> 
	/// A structure that contains information related to a MOUSE_MOVE event.
	/// </summary>
	struct MouseMoveEvent
	{
		/// <summary> 
		/// The new horizontal position of the cursor relative to the top-left corner of the client space of the window that sent the event.
		/// </summary>
		short m_x;

		/// <summary> 
		/// The new vertical position of the cursor relative to the top-left corner of the client space of the window that sent the event.
		/// </summary>
		short m_y;	
	};
}

#endif