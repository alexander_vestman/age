#ifndef AGE_WIN32_KEYS_HPP
#define AGE_WIN32_KEYS_HPP

//Undefine DELETE as there are conflicting names in the enumeration
#undef DELETE

namespace AGE
{
	/// <summary> 
	/// An enumeration of the various keys on the keyboard. Used for checking states of keys with the KeyboardState class and in keyboard related events.
	/// </summary>
	enum Key
	{
		BACKSPACE = 0x08,
		TAB = 0x09,
		RETURN = 0x0D,
		LINEFEED = 0x0A,

		SHIFT = 0x10,
		CONTROL = 0x11,
		ALT = 0x12,
		PAUSE = 0x13, 
		CAPS_LOCK = 0x14,
		ESPACE = 0x1B,

		SPACE = 0x20,
		PAGE_UP = 0x21,
		PAGE_DOWN = 0x22,
		END = 0x23,
		HOME = 0x24,
		LEFT = 0x25,
		UP = 0x26,
		RIGHT = 0x27,
		DOWN = 0x28,		
		PRINT_SCREEN = 0x2C,
		INSERT = 0x2D,
		DELETE = 0x2E,	

		ZERO = 0x30,
		ONE = 0x31,
		TWO = 0x32,
		THREE = 0x33,
		FOUR = 0x34,
		FIVE = 0x35,
		SIX = 0x36,
		SEVEN = 0x37,
		EIGHT = 0x38,
		NINE = 0x39,

		A = 0x41,
		B = 0x42,
		C = 0x43,
		D = 0x44,
		E = 0x45,
		F = 0x46,
		G = 0x47,
		H = 0x48,
		I = 0x49,
		J = 0x4A,
		K = 0x4B,
		L = 0x4C,
		M = 0x4D,
		N = 0x4E,
		O = 0x4F,
		P = 0x50,
		Q = 0x51,
		R = 0x52,
		S = 0x53,
		T = 0x54,
		U = 0x55,
		V = 0x56,
		W = 0x57,
		X = 0x58,
		Y = 0x59,
		Z = 0x5A,

		LEFT_SUPER = 0x5B,
		RIGHT_SUPER = 0x5C,

		NUMPAD_0 = 0x60,
		NUMPAD_1 = 0x61,
		NUMPAD_2 = 0x62,
		NUMPAD_3 = 0x63,
		NUMPAD_4 = 0x64,
		NUMPAD_5 = 0x65,
		NUMPAD_6 = 0x66,
		NUMPAD_7 = 0x67,
		NUMPAD_8 = 0x68,
		NUMPAD_9 = 0x69,

		NUMPAD_MULTIPLY = 0x6A,
		NUMPAD_ADD = 0x6B,
		NUMPAD_SUBTRACT = 0x6D,
		NUMPAD_DECIMAL = 0x6E,
		NUMPAD_DIVIDE = 0x6F,

		F1 = 0x70,
		F2 = 0x71,
		F3 = 0x72,
		F4 = 0x73,
		F5 = 0x74,
		F6 = 0x75,
		F7 = 0x76,
		F8 = 0x77,
		F9 = 0x78,
		F10 = 0x79,
		F11 = 0x7A,
		F12 = 0x7B,		

		NUM_LOCK = 0x90,
		SCROLL_LOCK = 0x91,

		LEFT_SHIFT = 0xA0,
		RIGHT_SHIFT = 0xA1,
		LEFT_CONTROL = 0xA2,
		RIGHT_CONTROL = 0xA3,	
		LEFT_ALT = 0xA4,
		RIGFHT_ALT = 0xA5,
		
		PLUS = 0xBB,
		COMMA = 0xBC,
		MINUS = 0xBD,
		PERIOD = 0xBE		
	};
}

//Redefine DELETE so we do not cause problems in other parts of the application
#define DELETE (0x00010000L)

#endif