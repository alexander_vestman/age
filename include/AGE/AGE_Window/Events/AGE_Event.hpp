#ifndef AGE_EVENT_HPP
#define AGE_EVENT_HPP

//General includes
#include <AGE/AGE_Initialize.h>

//Event includes
#include <AGE/AGE_Window/Events/AGE_KeyEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_CharEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_MouseMoveEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_MouseInputEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_MouseRawEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_MoveEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_FocusEvent.hpp>
#include <AGE/AGE_Window/Events/AGE_ResizeEvent.hpp>


namespace AGE
{
	/// <summary></summary>
	/// An enumeration contatining all different kinds of available event.
	/// </summary>
	enum class EventType: unsigned char
	{
		/// <summary> A key on the keyboard has changed state. </summary>
		KEY_CHANGE,
		/// <summary> The mouse has moved atleast one pixel. </summary>
		MOUSE_MOVE,
		/// <summary> The mouse sent raw input data that may be less than what is required to move the cursor. These event are also sent even when the mouse does not move from the input, e.g when in a corner. </summary>
		MOUSE_RAW,		
		/// <summary> A button on the mouse changed state. </summary>
		MOUSE_BUTTON,
		/// <summary> An event containing a translated UTF-8 unicode character resulting from of a key press. There may be several of events sent in a row to describe a single UTF-8 character. If there are more than one event for a character these must be pieced together. </summary>
		CHARACTER,
		/// <summary> Sent when a window has changed size. </summary>
		RESIZE,
		/// <summary> Sent when a window gains or loses focus from the user. </summary>
		FOCUS,
		/// <summary> Sent when a window has changed position. </summary>
		MOVE,
		/// <summary> Sent before the window is about to close. </summary>
		QUIT		
	};

	/// <summary>
	/// Class describing an event.  
	/// </summary>
	struct Event
	{	
		/// <summary>
		/// Describes which event type the current event is storing.
		/// </summary> 
		EventType m_type;
		union
		{
			AGE::KeyEvent			m_keyEvent;
			AGE::CharEvent			m_charEvent;
			AGE::ResizeEvent		m_resizeEvent;
			AGE::MoveEvent			m_moveEvent;
			AGE::MouseMoveEvent		m_mouseMoveEvent;
			AGE::MouseInputEvent	m_mouseInputEvent;
			AGE::FocusEvent			m_focusEvent;
			AGE::MouseRawEvent		m_mouseRawEvent;
		};	
	};
}


#endif