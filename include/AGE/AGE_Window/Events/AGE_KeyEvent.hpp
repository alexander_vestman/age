#ifndef AGE_KEY_EVENT_HPP
#define AGE_KEY_EVENT_HPP

#ifdef _WIN32
#	include <AGE/AGE_Window/Events/Win32/AGE_Win32_Keys.hpp>
#endif

namespace AGE
{
	/// <summary> 
	/// An enumeration describing the state of a key. Either up or down. 
	/// </summary>
	enum KeyState
	{		
		KEY_UP,
		KEY_DOWN
	};

	/// <summary> 
	/// A Struct containing information about a key on the keyboard. 
	/// </summary>
	struct KeyInfo
	{
		/// <summary> 
		/// A Struct containing information about a key on the keyboard. 
		/// </summary>
		unsigned short m_repeatCount;

		/// <summary> 
		/// The hardware scan code of the key. Each key has a unqiue code.
		/// </summary>
		unsigned char m_scanCode;

		/// <summary> 
		/// Contains information how the KeyState was before it changed to the current KeyState.
		/// </summary>
		AGE::KeyState m_previousState;

		/// <summary> 
		/// Contains information on the current KeyState.
		/// </summary>
		AGE::KeyState m_keyState;
	};	

	/// <summary> 
	/// A structure that contains information related to a KEY_CHANGE event.
	/// </summary>
	struct KeyEvent
	{			
		/// <summary> 
		/// Contains information on the key that changed state.
		/// </summary>
		AGE::KeyInfo m_keyInfo;

		/// <summary> 
		/// The key that changed state.
		/// </summary>
		AGE::Key m_key;		
	};
}

#endif