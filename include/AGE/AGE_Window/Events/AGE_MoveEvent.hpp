#ifndef AGE_MOVE_EVENT_HPP
#define AGE_MOVE_EVENT_HPP

namespace AGE
{
	/// <summary> 
	/// A structure that contains information related to a MOVE event.
	/// </summary>
	struct MoveEvent
	{
		/// <summary> 
		/// The new horizontal position of the window relative to the top-left corner of the screen.
		/// </summary>
		unsigned int m_x;

		/// <summary> 
		/// The new vertical position of the window relative to the top-left corner of the screen.
		/// </summary>
		unsigned int m_y;		
	};
}

#endif