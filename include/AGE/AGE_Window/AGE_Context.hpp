#ifndef AGE_CONTEXT_HPP
#define AGE_CONTEXT_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/AGE_ContextAttributes.hpp>

namespace AGE
{
	class AbstractContext;
}

namespace AGE
{
	class AGE_DLL Context
	{
	public:
		Context(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes);
		~Context();

	private:
		Context(const Context&);
		const Context& operator=(const Context&);
		AGE::AbstractContext* mp_abstractContext;
	};

}


#endif