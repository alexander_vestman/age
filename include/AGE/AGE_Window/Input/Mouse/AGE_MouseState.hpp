#ifndef AGE_MOUSE_STATE_HPP
#define AGE_MOUSE_STATE_HPP

#include <AGE/AGE_Window/Events/AGE_MouseInputEvent.hpp>

namespace AGE
{
	class MouseState
	{
	public:
		MouseState()
		{
			for(int i=0; i < m_buttonStateBufferSize; ++i)
			{
				m_buttons[i] = AGE::ButtonState::BUTTON_UP;
			}
			m_rawDeltaX = 0;
			m_rawDeltaY = 0;
		}

		/// <summary>
		/// Returns true if the specified button is down.
		/// </summary>
		/// <param name="button"> The Button to check if it is down. </param>
		bool isButtonDown(const AGE::Button& button)
		{
			return m_buttons[static_cast<unsigned int>(button)] == AGE::ButtonState::BUTTON_DOWN;
		}

		/// <summary>
		/// Returns true if the specified button is up.
		/// </summary>
		/// <param name="Key"> The Button to check if it is up. </param>
		bool isButtonUp(const AGE::Button& button)
		{
			return m_buttons[static_cast<unsigned int>(button)] == AGE::ButtonState::BUTTON_UP;
		}

		/// <summary>
		/// Returns true if the specified button is double clicked.
		/// </summary>
		/// <param name="button"> The Button to check if it is double clicked. </param>
		bool isButtonDoubleClick(const AGE::Button& button)
		{
			return m_buttons[static_cast<unsigned int>(button)] == AGE::ButtonState::BUTTON_DOUBLE_CLICK;
		}

		/// <summary>
		/// Sets the specified Button with the specified ButtonState value.
		/// </summary>
		/// <param name="button"> The button that is to be assigned the ButtonState value. </param>
		/// <param name="buttonState"> The value that is to be set to the specified Button. </param>
		void setButtonState(const AGE::Button& button, const AGE::ButtonState& buttonState)
		{
			m_buttons[static_cast<unsigned int>(button)] = buttonState;
		}

		/// <summary>
		/// The accumulated change of the raw input in the horizontal direction.
		/// </summary>
		long m_rawDeltaX;

		/// <summary>
		/// The accumulated change of the raw input in the vertical direction.
		/// </summary>
		long m_rawDeltaY;

	private:
		static const int m_buttonStateBufferSize = 5;
		AGE::ButtonState m_buttons[m_buttonStateBufferSize];
		
	};
}

#endif