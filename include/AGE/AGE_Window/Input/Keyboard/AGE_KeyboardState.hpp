#ifndef AGE_KEYBOARD_STATE_HPP
#define AGE_KEYBOARD_STATE_HPP

#include <AGE/AGE_Window/Events/AGE_KeyEvent.hpp>

namespace AGE
{
	class KeyboardState
	{
	public:
		KeyboardState()
		{
			for(int i=0; i < m_keyInfoBufferSize; ++i)
			{
				m_keys[i].m_keyState = AGE::KeyState::KEY_UP;
				m_keys[i].m_previousState = AGE::KeyState::KEY_UP;
				m_keys[i].m_repeatCount = 0;
				m_keys[i].m_scanCode = 0;
			}
		}

		/// <summary>
		/// Returns true if the specified key is down in the current state.
		/// </summary>
		/// <param name="key"> The Key to check if it is down. </param>
		bool isKeyDown(const AGE::Key& key)
		{
			return m_keys[static_cast<unsigned int>(key)].m_keyState == AGE::KeyState::KEY_DOWN;
		}

		/// <summary>
		/// Returns true if the specified key is up in the current state.
		/// </summary>
		/// <param name="key"> The Key to check if it is up. </param>
		bool isKeyUp(const AGE::Key& key)
		{
			return m_keys[static_cast<unsigned int>(key)].m_keyState == AGE::KeyState::KEY_UP;
		}

		/// <summary>
		/// Returns true if the specified key is down but was up in the previous state
		/// </summary>
		/// <param name="key"> The Key to check if it is pressed. </param>
		bool isKeyPressed(const AGE::Key& key)
		{
			return m_keys[static_cast<unsigned int>(key)].m_keyState == AGE::KeyState::KEY_DOWN && this->m_keys[static_cast<unsigned int>(key)].m_previousState == AGE::KeyState::KEY_UP;
		}

		/// <summary>
		/// Returns the amount of repeats the specified key has made while in the down state. Returns 0 when the key is up
		/// </summary>
		/// <param name="key"> The Key to get repeat count off. </param>
		unsigned short getRepeatCount(const AGE::Key& key)
		{
			return m_keys[static_cast<unsigned int>(key)].m_repeatCount;
		}

		/// <summary>
		/// Sets the KeyInfo of the specified key
		/// </summary>
		/// <param name="key"> The key that is to be assigned the KeyInfo value. </param>
		/// <param name="keyInfo"> The value that is to be set to the specified Key. </param>
		void setKeyState(const AGE::Key& key,const AGE::KeyInfo& keyInfo)
		{
			m_keys[static_cast<unsigned int>(key)] = keyInfo;
		}

	private:
		static const int m_keyInfoBufferSize = 256;
		AGE::KeyInfo m_keys[m_keyInfoBufferSize];
	};
}

#endif