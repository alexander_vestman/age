#ifndef AGE_KEYBOARD_HPP
#define AGE_KEYBOARD_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Input/Keyboard/AGE_KeyboardState.hpp>

namespace AGE
{
	/// <summary>
	/// A static class used for retrieving the state of the keyboard.
	/// </summary>
	class AGE_DLL Keyboard
	{
	public:
		/// <summary>
		/// Retrivies the current state of the keys on the keyboard as a KeyboardState object.
		/// </summary>
		static AGE::KeyboardState getKeyboardState();

		/// <summary>
		/// Sets the specified Key with the specified KeyInfo value. Generally this function is called by the default event handler of a window.
		/// </summary>
		/// <param name="key"> The key that is to be assigned the KeyInfo value. </param>
		/// <param name="keyInfo"> The value that is to be set to the specified Key. </param>
		static void setKeyState(const AGE::Key& key, const AGE::KeyInfo& keyInfo);

		/// <summary>
		/// Needs to be called to flip the internal buffer to make the current state of the keyboard readable. 
		/// </summary>
		static void Capture();	
	private:
		Keyboard();
	};
}

#endif
