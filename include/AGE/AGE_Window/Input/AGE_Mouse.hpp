#ifndef AGE_MOUSE_HPP
#define AGE_MOUSE_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Input/Mouse/AGE_MouseState.hpp>

namespace AGE
{
	/// <summary>
	/// A static class used for retrieving the state of the mouse.
	/// </summary>
	class AGE_DLL Mouse
	{
	public:
		/// <summary>
		/// Retrivies the current state of the buttons and values of the mouse as a MouseState object.
		/// </summary>
		static AGE::MouseState getMouseState();

		/// <summary>
		/// Sets the specified Button with the specified ButtonState value. Generally this function is called by the default event handler of a window.
		/// </summary>
		/// <param name="button"> The button that is to be assigned the ButtonState value. </param>
		/// <param name="buttonState"> The value that is to be set to the specified Button. </param>
		static void setButtonState(const AGE::Button& button, const AGE::ButtonState& buttonState);

		/// <summary>
		/// Adds the specifed amounts to the change in the raw input of the mouse.
		/// </summary>
		/// <param name="x"> The horizontal change of the raw value to be added. </param>
		/// <param name="y"> The vertical change of the raw value to be added. </param>
		static void addRawDelta(const long x, const long y);

		/// <summary>
		/// Needs to be called to flip the internal buffer to make the current state of the mouse readable. 
		/// </summary>
		static void Capture();
	private:
		Mouse();
	};
}

#endif
