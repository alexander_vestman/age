//General definitions and typedefines

//Platform definitions and typedefines
#ifdef _WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <Windows.h>
namespace AGE
{
	typedef HWND WindowHandle;
	typedef HICON IconHandle;
	typedef HCURSOR CursorHandle;
}
#endif