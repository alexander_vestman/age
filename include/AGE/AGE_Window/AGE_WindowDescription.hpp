#ifndef AGE_WINDOWDESCRIPTION_HPP
#define AGE_WINDOWDESCRIPTION_HPP

namespace AGE
{
	/// <summary>
	/// An enumeration that is used for setting the display mode of a window. Values from this enumeration should not be combined with eachother.
	/// </summary>
	enum class WindowMode
	{
		Windowed,
		WindowedFullscreen,
		Fullscreen
	};
	
	/// <summary>
	/// An enumeration that is used for setting the visual look of a window when it is in windowed display mode.
	/// Values from this enumeration can be combined to create desired window styles.
	/// </summary>
	enum WindowStyle
	{
		None		= 0,
		Border		= (1 << 0),
		Resize		= (1 << 1) | Border,
		Minimize	= (1 << 2) | Border,
		Caption		= (1 << 3) | Border,
		Close		= (1 << 4) | Border | Caption,		

		Default	= Border | Resize | Minimize | Caption | Close
	};

	/// <summary>
	/// A struct that is used for setting miscellaneous data of a window.
	/// </summary>
	struct WindowDescription
	{	
		char* m_windowCaption;		
	};

	/// <summary>
	/// A struct that is used for setting display related data of a window
	/// </summary>
	struct VideoDescription
	{
		unsigned short m_width;
		unsigned short m_height;
	};
}

#endif