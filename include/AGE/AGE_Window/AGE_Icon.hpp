#ifndef AGE_ICON_HPP
#define AGE_ICON_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>

namespace AGE
{
	/// <summary>
	/// An Icon abstraction that creates a platform specfic icon handle from pixel data.
	/// If the Icon object is deallocated or goes out of scope while being used by a window the behaviour is undefined.
	/// </summary>
	class AGE_DLL Icon
	{
	public:
		/// <summary>
		/// Creates an icon from the specified parameters.
		/// </summary>
		/// <param name="iconPixelData"> A pointer to data containing 32-bit RGBA formatted pixel data. The data must contain Width times Height RGBA tupples. </param>
		/// <param name="width"> The Width of the icon. </param>
		/// <param name="height"> The Height of the icon. </param>
		Icon(const unsigned char* iconPixelData, const int width, const int height);
		~Icon();

		/// <summary>
		/// Retrieves the platform specific IconHandle 
		/// </summary>
		inline AGE::IconHandle getIconHandle() const
		{
			return m_iconHandle;
		}

	private:
		AGE::IconHandle m_iconHandle;
	};
}

#endif