#ifndef AGE_WINDOW_HPP
#define AGE_WINDOW_HPP

//Standard includes
#include <functional>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Events/AGE_Event.hpp>
#include <AGE/AGE_Window/AGE_WindowDescription.hpp>
#include <AGE/AGE_Window/AGE_ContextAttributes.hpp>
#include <AGE/AGE_Window/AGE_Cursor.hpp>
#include <AGE/AGE_Window/AGE_Icon.hpp>

//Forward declarations
namespace AGE
{
	class AbstractWindow;
	class AbstractContext;
}

namespace AGE
{
	/// <summary>
	/// A Window abstraction that runs event handling in a separate thread.
	/// </summary>
	class AGE_DLL Window
	{
	public:
		/// <summary>
		/// Prepares a window to be created. To create the window fully <see cref="Create"> Create </see> must be called.
		/// </summary>
		Window();

		Window(AGE::Window&&);
		/// <summary>
		///	Creates a window with the specified parameters. The window will begin to run once created. 
		/// Blocks the calling thread until the window is fully created and ready to be used.
		/// </summary>
		/// <param name="ContextAttributes"> A copy of ContextAttriutes structure. Contains settings for the rendering context of the window. </param>
		///	<param name="WindowDescription"> A copy of WindowDescription structure that must have all values initialized. </param>
		/// <param name="VideoDescription"> A copy of a VideDescription structure. All values must be intialized or the behaviour is undefined. </param>
		/// <param name="WindowMode"> A value from the WindowMode enumeration. Combining enumerated values results in undefined behaviour. </param>
		/// <param name="WindowStyle"> A value from the WindowStyle enumeration. Combining enumerated values is possible. Defaults to the default window style. </param>
		Window(AGE::ContextAttributes ContextAttributes, AGE::WindowDescription WindowDescription, AGE::VideoDescription VideoDescription, AGE::WindowMode WindowMode, AGE::WindowStyle WindowStyle = AGE::WindowStyle::Default);		

		~Window();
		
		/// <summary>
		/// Creates a window with the specified parameters. If the window is already created it will first be destroyed and then recreated with the new parameters. 
		/// Blocks the calling thread until the window is fully created and ready to be used.
		/// </summary>
		/// <param name="ContextAttributes"> A copy of ContextAttriutes structure. Contains settings for the rendering context of the window. </param>
		///	<param name="WindowDescription"> A copy of WindowDescription structure that must have all values initialized. </param>
		/// <param name="VideoDescription"> A copy of a VideDescription structure. All values must be intialized or the behaviour is undefined. </param>
		/// <param name="WindowMode"> A value from the WindowMode enumeration. Combining enumerated values results in undefined behaviour. </param>
		/// <param name="WindowStyle"> A value from the WindowStyle enumeration. Combining enumerated values is possible. Defaults to the default window style. </param>
		/// 
		void Create(AGE::ContextAttributes ContextAttributes, AGE::WindowDescription WindowDescription, AGE::VideoDescription VideoDescription, AGE::WindowMode WindowMode, AGE::WindowStyle WindowStyle = AGE::WindowStyle::Default);

		/// <summary>
		/// Closes an opened window and frees any used resources. If the window is not created nothing will happen. 
		/// </summary>
		void Destroy();

		/// <summary>
		/// Swaps the front and backbuffer of this window. 
		/// </summary>
		void SwapBuffer();

		/// <summary>
		/// Makes the window's rendering context the currently active context on the thread this function is called from. Rendering related functions on this thread will now affect this window.
		/// </summary>
		void MakeCurrent();

		/// <summary>
		/// Sets the callback function that will be called when an event is processed by the window. The usage of a callback function is optional.
		/// When a callback function is specified the window will still use the default event handler to process events.
		/// The callback will be called from the thread that runs the event handling of the window and not from the main thread. 
		/// Only one callback function can be specified and any previous callback function will no longer be called.
		/// </summary>
		/// <param name="EventCallBack"> The callback function that will be called when an event occurs. </param> 
		void setEventCallback(std::function<void(const AGE::Event)> EventCallback);

		/// <summary>
		/// Sets the caption of a created window. If the caption is not UTF-8 formatted the function will fail. If the window is not created nothing will happen.
		/// </summary>
		/// <param name="Caption"> The new caption in a null terminated UTF-8 formatted string. </param>
		void setCaption(const char* Caption);

		/// <summary>
		/// Sets the icon of the window. If the Icon object was created from bad values the function will fail. If the window is not created nothing will happen.
		/// If the Icon object is destroyed while being the current icon of a window the behaviour is undefined.
		/// </summary>
		/// <param name="Icon"> An Icon object that contains the underlying icon handle. </param>
		void setIcon(const AGE::Icon& Icon);

		/// <summary>
		/// Sets how the cursor will look when it is over the window. If the Cursor object was created from bad values the function will fail. If the window is not created nothing will happen.
		/// If the Cursor object is destroyed while being the current cursor look of a window the behaviour is undefined.
		/// </summary>
		/// <param name="Cursor"> A Cursor object that contains the underlying cursor handle. </param>
		void setCursor(const AGE::Cursor& Cursor);

		/// <summary>
		/// Sets the position of the top-left corner of the window to the specified position. Does not resize the window. If the window is not created nothing will happen.
		/// </summary>
		/// <param name="X"> The new horizontal position of the window in pixels relative to the left side of the screen. </param>
		/// <param name="Y"> The new vertical position of the window in pixels relative to the top of the screen. </param>
		void setWindowPosition(const int X, const int Y);

		/// <summary>
		/// Sets the client area of a window to the specified size. Does not reposition the window. If the window is not created nothing will happen.
		/// </summary>
		/// <param name="X"> The new horizontal size of the client area in pixels. </param>
		/// <param name="Y"> The new vertical size of the client area in pixels. </param>
		void setWindowSize(const int Width, const int Height);

		/// <summary>
		/// Sets the mode of the window. The window will preserve the specified style when switching between fullscreen and windowed mode. If the window is not created nothing will happen.
		/// </summary>
		/// <param name="WindowMode"> A value from the WindowMode enumeration. Combining the enumerated values will result in undefiend behaviour. </param>
		void setWindowMode(const AGE::WindowMode& WindowMode);

		/// <summary>
		/// Checks if the window is open. 
		/// Returns true if the window is created and running. 
		/// Returns false if the window has been closed but not deallocated, e.g window was closed but <see cref="Destroy"> Destroy </see> has not been called.
		/// Returns false if the window has been deallocated. Returns false of the window has not been created.
		/// </summary>
		bool isOpen() const;

	private:		
		Window(const AGE::Window&);
		const AGE::Window& operator=(const AGE::Window&);

		AGE::AbstractWindow* mp_abstractWindow;
		AGE::AbstractContext* mp_abstractContext;
	};
}

#endif