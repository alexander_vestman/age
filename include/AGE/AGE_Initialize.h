
//General definitions and typedefines
#ifdef __cplusplus
#	define AGE_EXTERN extern "C"
#endif

//Platform definitions and typedefines
#ifdef _WIN32
#	ifdef _MSC_VER
#		ifdef AGE_BUILD
#			define AGE_DLL __declspec(dllexport)
#		else
#			define AGE_DLL __declspec(dllimport)
#		endif
#		define AGE_ALIGN(ALIGNMENT) __declspec(align(ALIGNMENT)) 
#	endif
#	define AGE_SYSTEM_WINDOWS
#endif


