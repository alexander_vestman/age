#include <AGE/AGE.h>

#include <thread>

#include <gl/GL.h>
#include <gl/GLU.h>

#include <iostream>

int main(int argc, char** argv)
{
	AGE::ContextAttributes contextAttributes;
	contextAttributes.m_antiAliasing = 0;
	contextAttributes.m_colorBitsPerPixel = 32;
	contextAttributes.m_contextProfile = AGE::ContextProfile::CORE_PROFILE;
	contextAttributes.m_contextFlags = AGE::ContextFlags::NO_FLAGS;
	contextAttributes.m_depthBitsPerPixel = 32;
	contextAttributes.m_majorVersion = 3;
	contextAttributes.m_minorVersion = 0;
	contextAttributes.m_stencilBitsPerPixel = 8;
	

	AGE::WindowDescription windowDesc;
	windowDesc.m_windowCaption = argv[argc-1];

	AGE::VideoDescription videoDesc;
	videoDesc.m_width		= 800;
	videoDesc.m_height		= 600;	
	
	AGE::WindowMode windowMode = AGE::WindowMode::Windowed;
	AGE::WindowStyle windowStyle = static_cast<AGE::WindowStyle>(AGE::WindowStyle::Caption | AGE::WindowStyle::Close | AGE::WindowStyle::Minimize);

	AGE::Window window = AGE::Window(contextAttributes, windowDesc, videoDesc, windowMode, windowStyle);

	window.MakeCurrent();	

	while(window.isOpen())
	{		
		AGE::Keyboard::Capture();
		AGE::Mouse::Capture();
		
		if(AGE::Keyboard::getKeyboardState().isKeyPressed(AGE::Key::A))
		{			
			window.Destroy();
		}				

		window.SwapBuffer();		

		std::this_thread::sleep_for(std::chrono::milliseconds(10));		
	}

	return 0;
}