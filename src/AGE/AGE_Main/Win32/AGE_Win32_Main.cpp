#include <AGE/AGE.h>

#include <string>
#include <vector>
#include <locale>

#include <Windows.h>

//Here we parse the windows command line into the standard
//argc and argv parts. All strings from here and onwards
//are treated as UTF-8 encoded.

int ParseCommandLine(const char* cmdLine, const int cmdLineLen, char** &argv)
{
	const char SPACE = char(32);
	const char QUOTE = char(34);	

	std::vector<std::string> argumentList;
	std::string currentArgument;

	for(int i=0; i < cmdLineLen; i++)
	{
		if(cmdLine[i] == QUOTE)
		{
			while((++i) < cmdLineLen && cmdLine[i] != QUOTE)
			{
				currentArgument += cmdLine[i];
			}
			if(!currentArgument.empty())
			{
				currentArgument += char(NULL);
				argumentList.push_back(currentArgument);
				currentArgument.clear();
			}
		}
		else if(cmdLine[i] == SPACE)
		{
			if(!currentArgument.empty())
			{
				currentArgument += char(NULL);
				argumentList.push_back(currentArgument);
				currentArgument.clear();
			}
		}
		else
		{
			currentArgument += cmdLine[i];
		}
	}
	if(!currentArgument.empty())
	{
		currentArgument += char(NULL);
		argumentList.push_back(currentArgument);
		currentArgument.clear();
	}

	argv = new char*[argumentList.size()];
	for(unsigned int i=0; i < argumentList.size(); i++)
	{
		argv[i] = new char[argumentList[i].size()];
		std::memcpy(argv[i], &(argumentList[i])[0], argumentList[i].size());
	}

	return static_cast<int>(argumentList.size());
}

//As we previously defined main as AGE_main we have to trick
//the preprocessor to write the standard main for us.
//In case the user program is not created as a Win32 application
//this will be the entry point.
#ifdef main
#	undef main
#endif
#define standard_main main

int standard_main(int argc, char** argv)
{
	return AGE_main(argc, argv);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	//The UTF-16 formated command line from windows
	LPWSTR wCmdLine = GetCommandLineW();
	int wCmdLineLen = wcslen(wCmdLine);

	int cmdLineLen = WideCharToMultiByte(CP_UTF8, WC_ERR_INVALID_CHARS, wCmdLine, wCmdLineLen, nullptr, 0, 0, 0);
	char* cmdLine = new char[cmdLineLen];
	WideCharToMultiByte(CP_UTF8, WC_ERR_INVALID_CHARS, wCmdLine, wCmdLineLen, cmdLine, cmdLineLen, 0, 0);

	int argc = 0;
	char** argv = nullptr;

	//Parse the arguments from the UTF-8 formated command line we extracted from windows
	argc = ParseCommandLine(cmdLine, cmdLineLen, argv);
		
	delete[] cmdLine;

	int result = 0;
	//Call the standard_main function to begin running the user program
	result = standard_main(argc, argv);

	//Cleanup of the arguments
	for(int i=0; i < argc; i++)
	{
		delete[] argv[i];
	}
	delete[] argv;

	return result;
}