#include <AGE/AGE_Window/Win32/AGE_Win32_Window.hpp>
#include <string>

namespace AGE
{	
	Win32Window::Win32Window(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle) : AbstractWindow()
	{	
		m_thread = std::thread([=](AGE::Win32Window* window)
		{	
			window->m_windowHandle = window->m_windowCreation.Create(contextAttributes, videoDescription, windowMode, windowStyle);
			window->m_windowConfig = AGE::WindowConfig(window->m_windowHandle, windowDescription); 

			window->mp_windowEventHandler = new AGE::WindowEventHandler(this, &window->m_windowConfig, &window->m_windowCreation);			

			//Set a pointer to WindowEventHandler as user data for the window so we can use it later in the Window Procedure without resorting to workarounds
			SetWindowLongW(window->m_windowHandle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(window->mp_windowEventHandler));

			window->m_windowConfig.setVisible(true);

			window->m_created = true;
			//Signal the Create function that the window is fully created
			m_createCondition.notify_all();

			MSG message;
			while(!window->mp_windowEventHandler->m_destroyEventPosted)
			{	
				//Retrieves every message currently in the message queue
				while(!window->mp_windowEventHandler->m_destroyEventPosted && PeekMessageW(&message, window->m_windowHandle, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&message);
					DispatchMessageW(&message);
				}			
				
				window->mp_windowEventHandler->PumpEvents();				

				//We do not want to lock the thread and wait for events if the window was destroyed in an event just before this
				if(!window->mp_windowEventHandler->m_destroyEventPosted)
				{
					//If the message queue is empty we suspend the thread until more messages need to be processed
					WaitMessage();
				}
			}

			//Perform cleanup

			window->m_windowCreation.Destroy();
			window->m_created = false;
			if(window->mp_windowEventHandler)
			{
				delete window->mp_windowEventHandler;	
				window->mp_windowEventHandler = nullptr;
			}		
			//Signal the Destroy function that we are about to exit
			m_createCondition.notify_all();	
		}, this);

		std::unique_lock<std::mutex> ReadyLock(m_createLock);
		m_createCondition.wait(ReadyLock);	
		
		m_thread.detach();
	}

	Win32Window::Win32Window(AGE::Win32Window&& rval)
	{	
		m_created.exchange(rval.m_created);		
		mp_windowEventHandler = std::move(rval.mp_windowEventHandler);	
		m_windowCreation = std::move(rval.m_windowCreation);		
		m_windowConfig = std::move(rval.m_windowConfig);
		m_windowHandle = rval.m_windowHandle;		
		m_thread = std::move(rval.m_thread);

		//Make sure that the right hand value does not destroy the window internals we just took
		rval.mp_windowEventHandler = nullptr;
		rval.m_created = false;
	}

	Win32Window::~Win32Window()
	{
		Destroy();
	}

	void Win32Window::Close()
	{
		if(m_created)
		{
			PostMessageW(m_windowHandle, WM_CLOSE, 0, 0);
		}
	}

	void Win32Window::Destroy()
	{
		if(m_created)
		{	
			//Force the Window thread to quit by setting DestroyEventPosted to true
			mp_windowEventHandler->m_destroyEventPosted = true;				
			//Post a message to the window in case it was hibernating
			PostMessageW(m_windowHandle, WM_CLOSE, 0, 0);
			m_created = false;
			
			//Wait for the thread to exit before proceding
			std::unique_lock<std::mutex> ReadyLock(m_createLock);
			m_createCondition.wait(ReadyLock);	
		}
	}

	void Win32Window::setEventCallback(std::function<void(const AGE::Event)> eventCallback)
	{
		mp_windowEventHandler->setEventCallback(eventCallback);
	}

	void Win32Window::setCaption(const char* caption)
	{
		m_windowConfig.setCaption(caption);
	}

	void Win32Window::setIcon(const AGE::Icon& icon)
	{
		m_windowConfig.setIcon(icon);
	}

	void Win32Window::setCursor(const AGE::Cursor& cursor)
	{
		m_windowConfig.setCursor(cursor);
	}

	void Win32Window::setWindowPosition(const int x, const int y)
	{
		m_windowConfig.setWindowPosition(x, y);
	}

	void Win32Window::setWindowSize(const int width, const int height)
	{
		m_windowConfig.setWindowSize(width, height);
	}

	void Win32Window::setWindowMode(const AGE::WindowMode& windowMode)
	{		
		m_windowCreation.setWindowMode(windowMode);
	}

	AGE::WindowHandle Win32Window::getWindowHandle() const
	{
		return m_windowHandle;
	}
}