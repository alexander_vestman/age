#include <AGE/AGE_Window/Win32/AGE_Win32_WindowConfig.hpp>

namespace AGE
{
	WindowConfig::WindowConfig()
	{
		m_windowHandle = nullptr;
		mp_cursor = nullptr;
	}

	WindowConfig::WindowConfig(WindowConfig&& rval)
	{
		m_windowHandle = rval.m_windowHandle;
		m_caption = std::move(rval.m_caption);
		mp_cursor = rval.mp_cursor;
	}

	WindowConfig::WindowConfig(AGE::WindowHandle windowHandle, AGE::WindowDescription windowDescription) 
	{
		m_windowHandle = windowHandle; 

		setCaption(windowDescription.m_windowCaption);
	}

	WindowConfig::~WindowConfig()
	{
		
	}

	void WindowConfig::setVisible(bool visible)
	{
		ShowWindow(m_windowHandle, visible ? SW_SHOW : SW_HIDE);
	}

	void WindowConfig::setCaption(const char* windowCaption)
	{
		int captionLen = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, windowCaption, -1, nullptr, 0);	
		wchar_t* wCaption = new wchar_t[captionLen];
		MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, windowCaption, -1, wCaption, captionLen);
		wCaption[captionLen-1] = wchar_t(0);

		m_caption = wCaption;

		delete[] wCaption;

		SendMessageW(m_windowHandle, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(&m_caption[0]));	
	}

	void WindowConfig::setIcon(const AGE::Icon& icon)
	{
		AGE::IconHandle iconHandle = icon.getIconHandle();
		if(iconHandle)
		{
			SendMessageW(m_windowHandle, WM_SETICON, ICON_BIG,   reinterpret_cast<LPARAM>(iconHandle));
			SendMessageW(m_windowHandle, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(iconHandle));
		}
	}

	void WindowConfig::setCursor(const AGE::Cursor& cursor)
	{
		mp_cursor = &cursor;
		SetCursor(cursor.getCursorHandle());
	}

	void WindowConfig::setWindowPosition(const int x, const int y)
	{
		SetWindowPos(m_windowHandle, nullptr, x, y, 0, 0, SWP_NOSIZE);
	}

	void WindowConfig::setWindowSize(const int width, const int height)
	{
		SetWindowPos(m_windowHandle, nullptr, 0, 0, width, height, SWP_NOMOVE);
	}

	void WindowConfig::RestoreCursor()
	{
		if(mp_cursor)
		{
			SetCursor(mp_cursor->getCursorHandle());
		}
	}
}