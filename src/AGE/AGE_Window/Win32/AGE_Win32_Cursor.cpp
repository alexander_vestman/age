#include <AGE/AGE_Window/AGE_Cursor.hpp>
#include <cstdint>

namespace AGE
{
	Cursor::Cursor(const unsigned char* cursorPixelData, const int width, const int height, const int hotSpotX, const int hotSpotY)
	{
		//Switches the red and blue channels
		std::uint8_t* data = new std::uint8_t[width * height * 4];
		for (int i = 0; i < width * height; ++i)
		{
			data[i * 4]		= cursorPixelData[i * 4 + 2];
			data[i * 4 + 1] = cursorPixelData[i * 4 + 1];
			data[i * 4 + 2] = cursorPixelData[i * 4 ];
			data[i * 4 + 3] = cursorPixelData[i * 4 + 3];
		}

		m_cursorHandle = CreateCursor(GetModuleHandleW(NULL), hotSpotX, hotSpotY, width, height, nullptr, data);
		//TODO: Add error handling and logging
		delete[] data;
	}

	Cursor::~Cursor()
	{
		if(m_cursorHandle)
		{
			DestroyCursor(m_cursorHandle);
		}
	}

}