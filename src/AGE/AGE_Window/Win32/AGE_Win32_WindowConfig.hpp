#ifndef AGE_WINDOWCONFIG_HPP
#define AGE_WINDOWCONFIG_HPP

//Standard includes
#include <string>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Icon.hpp>
#include <AGE/AGE_Window/AGE_Cursor.hpp>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/AGE_WindowDescription.hpp>

namespace AGE
{
	class WindowConfig 
	{
	public:
		WindowConfig();
		WindowConfig(WindowConfig&& rval);
		WindowConfig(AGE::WindowHandle windowHandle, AGE::WindowDescription	windowDescription);
		~WindowConfig();

		void setVisible(bool visible);
		void setCaption(const char* windowCaption);
		void setIcon(const AGE::Icon& icon);
		void setCursor(const AGE::Cursor& cursor);
		void setWindowPosition(const int x, const int y);
		void setWindowSize(const int width, const int height);		

		void RestoreCursor();

	private:
		AGE::WindowHandle	m_windowHandle; 
		const AGE::Cursor*	mp_cursor;
		std::wstring		m_caption;		
	};
}

#endif