#include <AGE/AGE_Window/Win32/AGE_WGLContext.hpp>
#include <windows.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include <AGE/AGE_Window/glext/wglext.h>


namespace AGE
{
	WGLContext::WGLContext(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes) : AGE::AbstractContext(contextAttributes)
	{
		m_windowHandle = windowHandle;
		m_deviceContext = GetDC(m_windowHandle);
		m_renderingContext = nullptr;
		m_defaultRenderingContext = nullptr;

		CreateDefaultContext(contextAttributes);
		CreateContext(contextAttributes);	
		DeleteDefaultContext();
	}

	WGLContext::WGLContext(AGE::WGLContext&& rval) : AGE::AbstractContext(m_contextAttributes)
	{
		m_contextAttributes = rval.m_contextAttributes;
		m_windowHandle = rval.m_windowHandle;		
		m_deviceContext = rval.m_deviceContext;		
		m_renderingContext = rval.m_renderingContext;

		rval.m_deviceContext = nullptr;
		rval.m_renderingContext = nullptr;
		rval.m_windowHandle = nullptr;
	}

	WGLContext::~WGLContext()
	{
		if(m_renderingContext)
		{
			wglDeleteContext(m_renderingContext);
			m_renderingContext = nullptr;
		}

		if(m_deviceContext)
		{
			ReleaseDC(m_windowHandle, m_deviceContext);
			m_deviceContext = nullptr;
		}
	}

	void WGLContext::SwapBuffer()
	{
		SwapBuffers(m_deviceContext);
	}

	void WGLContext::MakeCurrent()
	{
		wglMakeCurrent(m_deviceContext, m_renderingContext);
	}

	void WGLContext::CreateContext(const AGE::ContextAttributes& contextAttributes)
	{		
		if(contextAttributes.m_antiAliasing > 0)
		{			
			PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = reinterpret_cast<PFNWGLCHOOSEPIXELFORMATARBPROC>(wglGetProcAddress("wglChoosePixelFormatARB"));

			if(wglChoosePixelFormatARB)
			{
				const int pixelAttributes[] =
				{
					WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
					WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
					WGL_ACCELERATION_ARB,	WGL_FULL_ACCELERATION_ARB,
					WGL_DOUBLE_BUFFER_ARB,	GL_TRUE,
					WGL_PIXEL_TYPE_ARB,		WGL_TYPE_RGBA_ARB,
					WGL_COLOR_BITS_ARB,		contextAttributes.m_colorBitsPerPixel,
					WGL_DEPTH_BITS_ARB,		contextAttributes.m_depthBitsPerPixel,
					WGL_STENCIL_BITS_ARB,	contextAttributes.m_stencilBitsPerPixel,
					WGL_SAMPLE_BUFFERS_ARB, contextAttributes.m_antiAliasing ? GL_TRUE : GL_FALSE,
					WGL_SAMPLES_ARB,		contextAttributes.m_antiAliasing,
					NULL, NULL
				};

				int formats[128];
				unsigned int numberOfFormats;
				if(wglChoosePixelFormatARB(m_deviceContext, pixelAttributes, nullptr, 128, formats, &numberOfFormats))
				{
					if(numberOfFormats > 0)
					{
						bool validPixelFormatFound = false;

						for(unsigned int i = 0; i < numberOfFormats && !validPixelFormatFound; ++i)
						{
							PIXELFORMATDESCRIPTOR pixelFormatDescriptor;
							pixelFormatDescriptor.nSize = sizeof(pixelFormatDescriptor);
							pixelFormatDescriptor.nVersion = 1;
							DescribePixelFormat(m_deviceContext, formats[i], sizeof(pixelFormatDescriptor), &pixelFormatDescriptor);

							if(SetPixelFormat(m_deviceContext, formats[i], &pixelFormatDescriptor))
							{
								validPixelFormatFound = true;								
							}
						}

						if(!validPixelFormatFound)
						{
							//TODO: Add error report
						}
					}
					else
					{
						//TODO: Add error report
					}					
				}
				else
				{		
					
					//TODO: Add error report from GetLastError
				}
			}
			else
			{
				//TODO: Add error report wglChoosePixelFormat not supported
			}
		}
		else
		{
			PIXELFORMATDESCRIPTOR pixelFormatDescriptor = {0};
			pixelFormatDescriptor.nSize			= sizeof(pixelFormatDescriptor);
			pixelFormatDescriptor.nVersion		= 1;
			pixelFormatDescriptor.iLayerType	= PFD_MAIN_PLANE;
			pixelFormatDescriptor.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
			pixelFormatDescriptor.iPixelType	= PFD_TYPE_RGBA;
			pixelFormatDescriptor.cColorBits	= static_cast<unsigned char>(contextAttributes.m_colorBitsPerPixel);
			pixelFormatDescriptor.cDepthBits	= static_cast<unsigned char>(contextAttributes.m_depthBitsPerPixel);
			pixelFormatDescriptor.cStencilBits	= static_cast<unsigned char>(contextAttributes.m_stencilBitsPerPixel);
			pixelFormatDescriptor.cAlphaBits	= contextAttributes.m_colorBitsPerPixel == 32U ? 8U : 0U;

			if(!SetPixelFormat(m_deviceContext, ChoosePixelFormat(m_deviceContext, &pixelFormatDescriptor), &pixelFormatDescriptor))			
			{
				//TODO: Add error report
			}
		}

		if(contextAttributes.m_majorVersion >= 3)
		{
			PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(wglGetProcAddress("wglCreateContextAttribsARB"));

			if(wglCreateContextAttribsARB)
			{
				int attributes[] =
				{
					WGL_CONTEXT_MAJOR_VERSION_ARB, contextAttributes.m_majorVersion,
					WGL_CONTEXT_MINOR_VERSION_ARB, contextAttributes.m_minorVersion,
					WGL_CONTEXT_PROFILE_MASK_ARB, contextAttributes.m_contextProfile == ContextProfile::CORE_PROFILE ? WGL_CONTEXT_CORE_PROFILE_BIT_ARB : WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
					WGL_CONTEXT_FLAGS_ARB, static_cast<int>(contextAttributes.m_contextFlags),
					NULL, NULL
				};

				m_renderingContext = wglCreateContextAttribsARB(m_deviceContext, nullptr, attributes);				
			}
			else
			{
				//TODO: Add error report for wglCreateContextAttribsARB not supported
			}			
		}
		else
		{
			m_renderingContext = wglCreateContext(m_deviceContext);
		}

		if(!m_renderingContext)
		{
			//TODO: Add error report context creation failed
		}		
	}

	void WGLContext::CreateDefaultContext(const AGE::ContextAttributes& contextAttributes)
	{		
		int pixelFormat = 0;

		PIXELFORMATDESCRIPTOR pixelFormatDescriptor = {0};        
        pixelFormatDescriptor.nSize        = sizeof(pixelFormatDescriptor);
        pixelFormatDescriptor.nVersion     = 1;
        pixelFormatDescriptor.iLayerType   = PFD_MAIN_PLANE;
        pixelFormatDescriptor.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pixelFormatDescriptor.iPixelType   = PFD_TYPE_RGBA;
		pixelFormatDescriptor.cColorBits   = static_cast<unsigned char>(contextAttributes.m_colorBitsPerPixel);
		pixelFormatDescriptor.cDepthBits   = static_cast<unsigned char>(contextAttributes.m_depthBitsPerPixel);
		pixelFormatDescriptor.cStencilBits = static_cast<unsigned char>(contextAttributes.m_stencilBitsPerPixel);
        pixelFormatDescriptor.cAlphaBits   = contextAttributes.m_colorBitsPerPixel == 32U ? 8U : 0U;
        
        pixelFormat = ChoosePixelFormat(m_deviceContext, &pixelFormatDescriptor);
        if(pixelFormat)
        {
			if(SetPixelFormat(m_deviceContext, pixelFormat, &pixelFormatDescriptor))
			{
				m_defaultRenderingContext = wglCreateContext(m_deviceContext);
				if(m_defaultRenderingContext)
				{
					wglMakeCurrent(m_deviceContext, m_defaultRenderingContext);
				}
				else
				{
					//TODO: Add error report for failure to create basic OpenGL context
				}
			}
		}        	
		else
		{
			//TODO: Add error report
		}
	}

	void WGLContext::DeleteDefaultContext()
	{
		if(m_defaultRenderingContext)
		{
			if(!wglDeleteContext(m_defaultRenderingContext))
			{
				//TODO: Add error report from GetLastError()
			}
			m_defaultRenderingContext = nullptr;
		}
	}
}