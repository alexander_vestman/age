#include <AGE/AGE_Window/Win32/AGE_Win32_WindowCreation.hpp>
#include <AGE/AGE_Window/Win32/AGE_Win32_WindowEventHandler.hpp>
#include <sstream>

//Defines for registering raw input devices
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC         ((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE        ((USHORT) 0x02)
#endif

unsigned int AGE::WindowCreation::m_windowClassUsers = 0;


namespace AGE
{
	AGE::WindowCreation::WindowCreation()
	{
		m_windowClassCreated = false;		
		m_isFullscreen = false;
		m_isFocused = true;

		std::memset(&m_windowClass, 0, sizeof(WNDCLASSW));
	}

	AGE::WindowCreation::WindowCreation(WindowCreation&& rval)
	{
		m_windowClass = rval.m_windowClass;
		m_windowClassCreated = rval.m_windowClassCreated;
		m_windowClassName = std::move(rval.m_windowClassName);
		m_windowHandle = rval.m_windowHandle;

		rval.m_windowClassCreated = false;
	}

	AGE::WindowHandle WindowCreation::Create(const AGE::ContextAttributes& contextAttributes, const AGE::VideoDescription& videoDescription, const AGE::WindowMode& windowMode, const AGE::WindowStyle& windowStyle)
	{
		m_contextAttributes = contextAttributes;
		m_videoDescription = videoDescription;
		m_windowMode = windowMode;
		m_windowStyle = windowStyle;

		int windowWidth = videoDescription.m_width;
		int windowHeight = videoDescription.m_height;

		m_windowClassName = static_cast<std::wostringstream&>(std::wostringstream().flush() << m_windowClassUsers++).str().c_str(); 

		CreateWindowClass();

		DWORD style = WS_VISIBLE;

		if(windowMode == AGE::WindowMode::Windowed)
		{
			if(windowStyle == AGE::WindowStyle::None)
			{
				style |= WS_POPUP;
			}
			else
			{
				if((windowStyle & AGE::WindowStyle::Border)		== AGE::WindowStyle::Border)	{style |= WS_BORDER;}
				if((windowStyle & AGE::WindowStyle::Resize)		== AGE::WindowStyle::Resize)	{style |= WS_SIZEBOX | WS_MAXIMIZEBOX;}			
				if((windowStyle & AGE::WindowStyle::Minimize)	== AGE::WindowStyle::Minimize)	{style |= WS_MINIMIZEBOX;}			
				if((windowStyle & AGE::WindowStyle::Caption)	== AGE::WindowStyle::Caption)	{style |= WS_CAPTION;}
				if((windowStyle & AGE::WindowStyle::Close)		== AGE::WindowStyle::Close)		{style |= WS_SYSMENU;}
			}

			RECT windowRect = {0, 0, windowWidth, windowHeight};
			AdjustWindowRect(&windowRect, style, false);
			windowWidth		= windowRect.right	- windowRect.left;
			windowHeight	= windowRect.bottom - windowRect.top;
		}
		else if(windowMode == AGE::WindowMode::WindowedFullscreen || windowMode == AGE::WindowMode::Fullscreen)
		{
			style |= WS_POPUP;
		}		

		m_windowHandle = CreateWindowExW(0L, m_windowClassName.c_str(), nullptr, style, CW_USEDEFAULT, CW_USEDEFAULT, windowWidth, windowHeight, NULL, NULL, GetModuleHandle(NULL), NULL);	

		RAWINPUTDEVICE rawInputDevice;
		rawInputDevice.usUsagePage	= HID_USAGE_PAGE_GENERIC; 
		rawInputDevice.usUsage		= HID_USAGE_GENERIC_MOUSE; 
		rawInputDevice.dwFlags		= RIDEV_INPUTSINK;   
		rawInputDevice.hwndTarget	= m_windowHandle;
		RegisterRawInputDevices(&rawInputDevice, 1, sizeof(rawInputDevice));

		if(windowMode == AGE::WindowMode::Fullscreen)
		{
			setWindowMode(windowMode);
		}

        return m_windowHandle;
	}

	void WindowCreation::setWindowMode(const AGE::WindowMode& windowMode)
	{
		switch(windowMode)
		{
			case WindowMode::Fullscreen:
			{
				DEVMODE devMode			= {0};
				devMode.dmSize			= sizeof(DEVMODE);
				devMode.dmPelsWidth		= m_videoDescription.m_width;
				devMode.dmPelsHeight	= m_videoDescription.m_height;
				devMode.dmBitsPerPel	= m_contextAttributes.m_colorBitsPerPixel;
				devMode.dmFields		= DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

				ChangeDisplaySettingsW(&devMode, CDS_FULLSCREEN);

				SetWindowLongW(m_windowHandle, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
				SetWindowLongW(m_windowHandle, GWL_EXSTYLE, WS_EX_APPWINDOW);

				SetWindowPos(m_windowHandle, HWND_TOP, 0, 0, m_videoDescription.m_width, m_videoDescription.m_height, SWP_FRAMECHANGED);
				m_isFullscreen = true;
	
				break;
			}
			case WindowMode::WindowedFullscreen:
			{
				if(m_isFullscreen)
				{
					ChangeDisplaySettingsW(NULL, 0);
					m_isFullscreen = false;
				}

				SetWindowLongW(m_windowHandle, GWL_STYLE, WS_POPUP | WS_VISIBLE);
				SetWindowPos(m_windowHandle, HWND_TOP, 0, 0, m_videoDescription.m_width, m_videoDescription.m_height, SWP_NOMOVE);

				break;
			}
			case WindowMode::Windowed:
			{
				if(m_isFullscreen)
				{
					ChangeDisplaySettingsW(NULL, 0);
					m_isFullscreen = false;
				}

				DWORD style = WS_VISIBLE;

				if(m_windowStyle == AGE::WindowStyle::None)
				{
					style |= WS_POPUP;
				}
				else
				{
					if((m_windowStyle & AGE::WindowStyle::Border)		== AGE::WindowStyle::Border)	{style |= WS_BORDER;}
					if((m_windowStyle & AGE::WindowStyle::Resize)		== AGE::WindowStyle::Resize)	{style |= WS_SIZEBOX | WS_MAXIMIZEBOX;}			
					if((m_windowStyle & AGE::WindowStyle::Minimize)		== AGE::WindowStyle::Minimize)	{style |= WS_MINIMIZEBOX;}			
					if((m_windowStyle & AGE::WindowStyle::Caption)		== AGE::WindowStyle::Caption)	{style |= WS_CAPTION;}
					if((m_windowStyle & AGE::WindowStyle::Close)		== AGE::WindowStyle::Close)		{style |= WS_SYSMENU;}
				}

				RECT windowRect = {0, 0, m_videoDescription.m_width, m_videoDescription.m_height};
				AdjustWindowRect(&windowRect, style, false);
				int windowWidth		= windowRect.right	- windowRect.left;
				int windowHeight	= windowRect.bottom - windowRect.top;

				SetWindowPos(m_windowHandle, HWND_TOP, 0, 0, windowWidth, windowHeight, SWP_NOMOVE);
				break;
			}
		}
	}

	void WindowCreation::FocusChange(bool isLostFocus)
	{
		if(isLostFocus)
		{			
			if(m_isFullscreen && m_isFocused)
			{
				SendMessageW(m_windowHandle, WM_SYSCOMMAND, SC_MINIMIZE, 0);
				ChangeDisplaySettingsW(NULL, 0);
			}
			m_isFocused = false;
		}
		else
		{
			if(m_isFullscreen && !m_isFocused)
			{
				DEVMODE devMode			= {0};
				devMode.dmSize			= sizeof(DEVMODE);
				devMode.dmPelsWidth		= m_videoDescription.m_width;
				devMode.dmPelsHeight	= m_videoDescription.m_height;
				devMode.dmBitsPerPel	= m_contextAttributes.m_colorBitsPerPixel;
				devMode.dmFields		= DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

				ChangeDisplaySettingsW(&devMode, CDS_FULLSCREEN);						
			}
			m_isFocused = true;
		}
	}

	void WindowCreation::Destroy()
	{		
		if(m_isFullscreen)
		{
			ChangeDisplaySettingsW(NULL, 0);
			m_isFullscreen = false;
		}

		DestroyWindow(m_windowHandle);
		DestroyWindowClass();
	}

	void WindowCreation::CreateWindowClass()
	{
		m_windowClass.style				= 0;
		m_windowClass.lpfnWndProc		= &AGE::WindowEventHandler::WndProc;
		m_windowClass.cbClsExtra		= 0;
		m_windowClass.cbWndExtra		= 0;
		m_windowClass.hInstance			= GetModuleHandleW(NULL);
		m_windowClass.hIcon				= 0;
		m_windowClass.hCursor			= 0;
		m_windowClass.hbrBackground		= 0;
		m_windowClass.lpszMenuName		= 0;
		m_windowClass.lpszClassName		= m_windowClassName.c_str();

		RegisterClassW(&m_windowClass);
		m_windowClassCreated = true;
	}

	void WindowCreation::DestroyWindowClass()
	{
		if(m_windowClassCreated)
		{
			UnregisterClassW(m_windowClassName.c_str(), GetModuleHandleW(NULL));
			m_windowClassCreated = false;
		}
	}
}