#include <AGE/AGE_Window/Win32/AGE_Win32_WindowEventHandler.hpp>
#include <AGE/AGE_Window/Win32/AGE_Win32_WindowCreation.hpp>
#include <AGE/AGE_Window/Win32/AGE_Win32_WindowConfig.hpp>
#include <AGE/AGE_Window/AGE_AbstractWindow.hpp>
#include <windowsx.h>

namespace AGE
{
	WindowEventHandler::WindowEventHandler(AGE::AbstractWindow* window, AGE::WindowConfig* windowConfig, AGE::WindowCreation* windowCreation) :  AGE::AbstractWindowEventHandler(window, windowConfig, windowCreation)
	{

	}

	WindowEventHandler::~WindowEventHandler()
	{

	}

	LRESULT CALLBACK WindowEventHandler::WndProc(HWND hwnd, UINT uMsg , WPARAM wParam, LPARAM lParam)
	{
		AGE::WindowEventHandler* eventHandler = reinterpret_cast<AGE::WindowEventHandler*>(GetWindowLongW(hwnd, GWLP_USERDATA));

		if(eventHandler != nullptr)
		{
			AGE::Event event;
			switch(uMsg)
			{
				//Raw input 
				case WM_INPUT:
				{
					event.m_type = AGE::EventType::MOUSE_RAW;

					RAWINPUT RawInput;
					unsigned int RawInputSize = sizeof(RawInput);
					GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, &RawInput, &RawInputSize, sizeof(RAWINPUTHEADER));

					if(RawInput.header.dwType == RIM_TYPEMOUSE)
					{
						event.m_mouseRawEvent.m_x = RawInput.data.mouse.lLastX;
						event.m_mouseRawEvent.m_y = RawInput.data.mouse.lLastY;
						eventHandler->m_events.push_back(event);
					}

					//Call the default windows procedure to allow it to clean up as specified on MSDN
					return DefWindowProcW(hwnd, uMsg , wParam, lParam);
					break;
				}

				case WM_SETCURSOR:
				{
					if(LOWORD(lParam) == HTCLIENT)
					{
						eventHandler->mp_windowConfig->RestoreCursor();
					}
					break;
				}

				case WM_MOUSEMOVE:
				{
					event.m_type = AGE::EventType::MOUSE_MOVE;

					event.m_mouseMoveEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseMoveEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				//Left Mouse button
				case WM_LBUTTONDOWN:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::LEFT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOWN;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_LBUTTONUP:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::LEFT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_UP;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_LBUTTONDBLCLK:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::LEFT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOUBLE_CLICK;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				//Middle Mouse button
				case WM_MBUTTONDOWN:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::MIDDLE;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOWN;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_MBUTTONUP:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::MIDDLE;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_UP;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_MBUTTONDBLCLK:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::MIDDLE;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOUBLE_CLICK;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				//Right Mouse button
				case WM_RBUTTONDOWN:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::RIGHT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOWN;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_RBUTTONUP:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::RIGHT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_UP;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_RBUTTONDBLCLK:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = AGE::Button::RIGHT;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOUBLE_CLICK;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				//X Mouse buttons
				case WM_XBUTTONDOWN:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = (HIWORD(wParam) == 1) ? AGE::Button::X_BUTTON_ONE : AGE::Button::X_BUTTON_TWO;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOWN;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_XBUTTONUP:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = (HIWORD(wParam) == 1) ? AGE::Button::X_BUTTON_ONE : AGE::Button::X_BUTTON_TWO;	
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_UP;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_XBUTTONDBLCLK:
				{
					event.m_type = AGE::EventType::MOUSE_BUTTON;

					event.m_mouseInputEvent.m_button = (HIWORD(wParam) == 1) ? AGE::Button::X_BUTTON_ONE : AGE::Button::X_BUTTON_TWO;
					event.m_mouseInputEvent.m_state = AGE::ButtonState::BUTTON_DOUBLE_CLICK;

					event.m_mouseInputEvent.m_x = GET_X_LPARAM(lParam);
					event.m_mouseInputEvent.m_y = GET_Y_LPARAM(lParam);

					eventHandler->m_events.push_back(event);
					break;
				}


				case WM_KEYDOWN:				
				{
					/*
						wParam
						Key code for the pressed key

						lParam bit field
						0-15	The repeat count for the current message. 
						16-23	The scan code.
						24		Indicates whether the key is an extended key.
						25-28	Reserved; do not use.
						29		The context code.
						30		The previous key state.
						31		The transition state.
					*/

					event.m_type = AGE::EventType::KEY_CHANGE;

					event.m_keyEvent.m_key = static_cast<AGE::Key>(wParam);
					event.m_keyEvent.m_keyInfo.m_keyState = AGE::KeyState::KEY_DOWN;

					//Truncate all but the first 16 bits to extract repeat count
					event.m_keyEvent.m_keyInfo.m_repeatCount = static_cast<unsigned short>(lParam);

					//Truncate all but the first 8 bits after bitshifting 16 steps to the right
					event.m_keyEvent.m_keyInfo.m_scanCode = static_cast<unsigned char>((lParam >> 16) & 0xFFFF);

					//Bitwise AND operation to extract the 30th bit and compare it to (1U<<30), then cast it to a KeyState enumeration (KEY_UP or KEY_DOWN)						
					event.m_keyEvent.m_keyInfo.m_previousState = static_cast<AGE::KeyState>((static_cast<unsigned int>(lParam) & (1U<<30)) == (1U<<30)) ;

					eventHandler->m_events.push_back(event);
					break;
				}

				case WM_KEYUP:				
				{
					/*
						wParam
						Key code for the released key

						lParam bit field
						0-15	The repeat count for the current message. Always 1 for WM_KEYUP
						16-23	The scan code.
						24		Indicates whether the key is an extended key.
						25-28	Reserved; do not use.
						29		The context code.
						30		The previous key state. Always 1 for WM_KEYUP
						31		The transition state. Always down for WM_KEYUP
					*/
				
					event.m_type = AGE::EventType::KEY_CHANGE;

					event.m_keyEvent.m_key = static_cast<AGE::Key>(wParam);
					event.m_keyEvent.m_keyInfo.m_keyState = AGE::KeyState::KEY_UP;

					//Always 1 for WM_KEYUP
					event.m_keyEvent.m_keyInfo.m_repeatCount = 1;

					//Truncate all but the first 8 bits after bitshifting 16 steps to the right
					event.m_keyEvent.m_keyInfo.m_scanCode = static_cast<unsigned char>((lParam >> 16) & 0xFFFF);

					//Always KEY_DOWN KeyState
					event.m_keyEvent.m_keyInfo.m_previousState = AGE::KeyState::KEY_DOWN;				

					eventHandler->m_events.push_back(event);
					break;
				}

				//Character messages
				case WM_CHAR:
				{
					switch (wParam) 
					{ 
						//Handle non-displayable characters.
						case AGE::Key::BACKSPACE: 
						case AGE::Key::TAB: 
						case AGE::Key::LINEFEED: 					
						case AGE::Key::RETURN: 
						case AGE::Key::ESPACE:
							break;					
						default:
						{							
							event.m_type = AGE::EventType::CHARACTER;		

							wchar_t WCharBuffer[] = {static_cast<wchar_t>(wParam)};						

							int Utf8Len = WideCharToMultiByte(CP_UTF8, WC_ERR_INVALID_CHARS, WCharBuffer, 1, nullptr, 0, 0, 0);
							char* Utf8Buffer = new char[Utf8Len];
							WideCharToMultiByte(CP_UTF8, WC_ERR_INVALID_CHARS, WCharBuffer, 1, Utf8Buffer, Utf8Len, 0, 0);

							//Generate a CharEvent for each byte in the UTF-8 character
							for(int i=0; i < Utf8Len; ++i)
							{
								event.m_charEvent.m_character = (*(Utf8Buffer + i));
								eventHandler->m_events.push_back(event);
							}
							delete[] Utf8Buffer;
						}
					} 				
					break;
				}		

				case WM_MOVE:
				{
					/*
						wParam
						Unused

						lParam bit field
						0-15 (LOW WORD)		The new x position of the window.
						16-23 (HIGH WORD)	The new y position of the window.						
					*/	

					event.m_type = AGE::EventType::MOVE;

					event.m_moveEvent.m_x = static_cast<int>(LOWORD(lParam));
					event.m_moveEvent.m_y = static_cast<int>(HIWORD(lParam));

					eventHandler->m_events.push_back(event);
					break;
				}			
				case WM_SIZE:
				{
					/*
						wParam
						Size request type 

						lParam bit field
						0-15 (LOW WORD)		The new width of the window.
						16-23 (HIGH WORD)	The new height of the window.						
					*/				

					event.m_type = AGE::EventType::RESIZE;
					switch (wParam)
					{
						case SIZE_RESTORED:
						{
							event.m_resizeEvent.m_resizeType = AGE::ResizeType::RESIZED;
							break;
						}
						case SIZE_MINIMIZED:
						{
							event.m_resizeEvent.m_resizeType = AGE::ResizeType::MINIMIZED;
							break;
						}
						case SIZE_MAXIMIZED:
						{
							event.m_resizeEvent.m_resizeType = AGE::ResizeType::MAXIMIZED;
							break;
						}
					}

					event.m_resizeEvent.m_width = static_cast<int>(LOWORD(lParam));
					event.m_resizeEvent.m_height = static_cast<int>(HIWORD(lParam));

					eventHandler->m_events.push_back(event);
					break;
				}
				case WM_CLOSE:
				{
					event.m_type = AGE::EventType::QUIT;
					eventHandler->m_events.push_back(event);
					break;
				}
				
				case WM_SETFOCUS:
				{
					event.m_type = AGE::EventType::FOCUS;
					event.m_focusEvent.m_focusType = AGE::FocusType::GAIN;
					eventHandler->m_events.push_back(event);

					eventHandler->mp_windowCreation->FocusChange(false);
					break;
				}				
				case WM_KILLFOCUS:
				{			
					event.m_type = AGE::EventType::FOCUS;
					event.m_focusEvent.m_focusType = AGE::FocusType::LOSE;
					eventHandler->m_events.push_back(event);					

					eventHandler->mp_windowCreation->FocusChange(true);
					break;
				}
				default:
				{
					//Use the default window procedure for all events we do not process when the window is created
					return DefWindowProcW(hwnd, uMsg , wParam, lParam);
				}
			}
			return 1;
		}
		
		//Use the default window procedure for all events when the window is not created
		return DefWindowProcW(hwnd, uMsg , wParam, lParam);	
	}
}