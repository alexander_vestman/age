#ifndef AGE_WIN32_WINDOW_CREATION_HPP
#define AGE_WIN32_WINDOW_CREATION_HPP

//Standard includes
#include <string>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/AGE_WindowDescription.hpp>
#include <AGE/AGE_Window/AGE_ContextAttributes.hpp>

namespace AGE
{
	class WindowCreation
	{		
	public:	
		WindowCreation();
		WindowCreation(WindowCreation&& rval);
		AGE::WindowHandle Create(const AGE::ContextAttributes& contextAttributes, const AGE::VideoDescription& videoDescription, const AGE::WindowMode& windowMode, const AGE::WindowStyle& windowStyle);
		void setWindowMode(const AGE::WindowMode& windowMode);

		void FocusChange(bool isLostFocus);

		void Destroy();

	private:
		inline void CreateWindowClass();
		inline void DestroyWindowClass();

		AGE::WindowHandle m_windowHandle;
		AGE::ContextAttributes m_contextAttributes;
		AGE::VideoDescription m_videoDescription;
		AGE::WindowMode m_windowMode;		
		AGE::WindowStyle m_windowStyle;

		static unsigned int m_windowClassUsers;
		std::wstring m_windowClassName;
		WNDCLASSW m_windowClass;		
		bool m_windowClassCreated;
		bool m_isFullscreen;
		bool m_isFocused;
	};
}

#endif