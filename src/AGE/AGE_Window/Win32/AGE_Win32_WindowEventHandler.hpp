#ifndef AGE_WIN32_WINDOWEVENTHANDLER_HPP
#define AGE_WIN32_WINDOWEVENTHANDLER_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/AGE_AbstractWindowEventHandler.hpp>

//Forward declarations
namespace AGE
{
	class Window;
}

namespace AGE
{
	class WindowEventHandler : public AGE::AbstractWindowEventHandler
	{
	public:
		WindowEventHandler(AGE::AbstractWindow* window, AGE::WindowConfig* windowConfig, AGE::WindowCreation* windowCreation);
		~WindowEventHandler();

		static LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg , WPARAM wParam, LPARAM lParam);
	};
}

#endif