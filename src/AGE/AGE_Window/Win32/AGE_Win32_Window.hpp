#ifndef AGE_WIN32_WINDOW_HPP
#define AGE_WIN32_WINDOW_HPP

#include <AGE/AGE_Window/AGE_AbstractWindow.hpp>

namespace AGE
{
	class Win32Window : public AGE::AbstractWindow
	{
	public:
		Win32Window::Win32Window(AGE::Win32Window&& rval);
		Win32Window(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle);		
		~Win32Window() override;

		void Close() override;
		void Destroy() override;		

		void setEventCallback(std::function<void(const AGE::Event)> eventCallback) override;
		void setCaption(const char* caption) override;
		void setIcon(const AGE::Icon& icon) override;
		void setCursor(const AGE::Cursor& cursor) override;
		void setWindowPosition(const int x, const int y) override;
		void setWindowSize(const int width, const int height) override;
		void setWindowMode(const AGE::WindowMode& windowMode) override;

		AGE::WindowHandle getWindowHandle() const override;
	};
}

#endif