#ifndef AGE_WGL_CONTEXT_HPP
#define AGE_WGL_CONTEXT_HPP

#include <AGE/AGE_Window/AGE_AbstractContext.hpp>
 
namespace AGE
{
	class WGLContext : public AbstractContext
	{
	public:
		 WGLContext(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes);
		 ~WGLContext();
		 WGLContext(AGE::WGLContext&& rval);

		 void SwapBuffer() override;
		 void MakeCurrent() override;

	private:
		void CreateContext(const AGE::ContextAttributes& contextAttributes);
		void CreateDefaultContext(const AGE::ContextAttributes& contextAttributes);
		void DeleteDefaultContext();

		AGE::WindowHandle m_windowHandle;
		HDC m_deviceContext;
		HGLRC m_renderingContext;	

		HGLRC m_defaultRenderingContext;
	};
}

#endif