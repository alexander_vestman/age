#include <AGE/AGE_Window/AGE_Icon.hpp>
#include <cstdint>

namespace AGE
{
	Icon::Icon(const unsigned char* iconPixelData, const int width, const int height)
	{
		//Switches the red and blue channels
		std::uint8_t* data = new std::uint8_t[width * height * 4];
		for (int i = 0; i < width * height; ++i)
		{
			data[i * 4]		= iconPixelData[i * 4 + 2];
			data[i * 4 + 1] = iconPixelData[i * 4 + 1];
			data[i * 4 + 2] = iconPixelData[i * 4 ];
			data[i * 4 + 3] = iconPixelData[i * 4 + 3];
		}
			
		m_iconHandle = CreateIcon(GetModuleHandleW(NULL), width, height, 1, 32, nullptr, &data[0]);
		//TODO: Add error handling and logging
		delete[] data;
	}

	Icon::~Icon()
	{
		if(m_iconHandle)
		{
			DestroyIcon(m_iconHandle);
		}
	}
}