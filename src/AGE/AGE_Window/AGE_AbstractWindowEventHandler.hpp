#ifndef AGE_ABSTRACT_WINDOW_EVENT_HANDLER_HPP
#define AGE_ABSTRACT_WINDOW_EVENT_HANDLER_HPP

//Standard includes
#include <functional>
#include <vector>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/Events/AGE_Event.hpp>

//Forward declarations
namespace AGE
{
	class AbstractWindow;
	class WindowConfig;
	class WindowCreation;
}

namespace AGE
{
	class AbstractWindowEventHandler
	{
	public:		
		AbstractWindowEventHandler(AGE::AbstractWindow* window, AGE::WindowConfig* windowConfig, AGE::WindowCreation* windowCreation);
		virtual ~AbstractWindowEventHandler() = 0;

		void Event(const AGE::Event Event);
		void PumpEvents();

		void setEventCallback(std::function<void(const AGE::Event)> eventCallback);
		void RemoveEventInterceptor();

		void operator=(const AbstractWindowEventHandler& rhs);
	
		bool m_destroyEventPosted;

		void LockEventQueue();
		void UnlockEventQueue();

	protected:
		std::vector<AGE::Event> m_events;
		std::function<void(const AGE::Event)> m_eventCallback; 

		AGE::AbstractWindow* mp_window;
		AGE::WindowConfig* mp_windowConfig;
		AGE::WindowCreation* mp_windowCreation;

	private:		
		void EventSwitch(const AGE::Event& event);		
	};
}

#endif