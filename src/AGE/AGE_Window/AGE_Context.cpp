#include <AGE/AGE_Window/AGE_Context.hpp>
#include <AGE/AGE_Window/AGE_AbstractContext.hpp>

namespace AGE
{
	Context::Context(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes)
	{
		mp_abstractContext = AGE::AbstractContext::Create(windowHandle, contextAttributes);
	}

	Context::~Context()
	{
		delete mp_abstractContext;
	}
}