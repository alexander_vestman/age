#include <AGE/AGE_Window/AGE_AbstractWindowEventHandler.hpp>
#include <AGE/AGE_Window/AGE_AbstractWindow.hpp>
#include <AGE/AGE_Window/Input/AGE_Keyboard.hpp>
#include <AGE/AGE_Window/Input/AGE_Mouse.hpp>

namespace AGE
{
	AbstractWindowEventHandler::AbstractWindowEventHandler(AGE::AbstractWindow* window, AGE::WindowConfig* windowConfig, AGE::WindowCreation* windowCreation)
	{		
		mp_window = window;
		mp_windowConfig = windowConfig;
		mp_windowCreation = windowCreation;

		m_destroyEventPosted = false;
	}

	AbstractWindowEventHandler::~AbstractWindowEventHandler()
	{

	}

	void AbstractWindowEventHandler::Event(const AGE::Event event)
	{		
		m_events.push_back(event);
	}

	void AbstractWindowEventHandler::setEventCallback(std::function<void(AGE::Event)> eventCallback)
	{	
		m_eventCallback = eventCallback;
	}

	void AbstractWindowEventHandler::PumpEvents()
	{
		if(m_eventCallback)
		{
			for(std::vector<AGE::Event>::iterator EventIt = m_events.begin(); EventIt != m_events.end(); ++EventIt)
			{		
				EventSwitch(*EventIt);
				m_eventCallback((*EventIt));
			}
		}
		else
		{
			for(std::vector<AGE::Event>::iterator EventIt = m_events.begin(); EventIt != m_events.end(); ++EventIt)
			{
				EventSwitch(*EventIt);
			}
		}

		m_events.clear();
	}

	void AbstractWindowEventHandler::operator=(const AbstractWindowEventHandler& rhs)
	{
		m_destroyEventPosted = rhs.m_destroyEventPosted;
		mp_window = rhs.mp_window;
		mp_windowConfig = rhs.mp_windowConfig;
		mp_windowCreation = rhs.mp_windowCreation;
	}

	void AbstractWindowEventHandler::EventSwitch(const AGE::Event& event)
	{
		switch(event.m_type)
		{
			case AGE::EventType::MOUSE_RAW:
			{						
				AGE::Mouse::addRawDelta(event.m_mouseRawEvent.m_x, event.m_mouseRawEvent.m_y);
				break;
			}
			case AGE::EventType::KEY_CHANGE:
			{
				AGE::Keyboard::setKeyState(event.m_keyEvent.m_key, event.m_keyEvent.m_keyInfo);
				break;
			}
			case AGE::EventType::MOUSE_BUTTON:
			{
				AGE::Mouse::setButtonState(event.m_mouseInputEvent.m_button, event.m_mouseInputEvent.m_state);
				break;
			}			
			case AGE::EventType::QUIT:
			{						
				m_destroyEventPosted = true;
				break;
			}	
		}
	}
	
}