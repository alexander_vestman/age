#include <AGE/AGE_Window/AGE_AbstractContext.hpp>


#if defined(AGE_SYSTEM_WINDOWS)

#include <AGE/AGE_Window/Win32/AGE_WGLContext.hpp>
typedef AGE::WGLContext AbstractContextImplementation;

#endif

namespace AGE
{
	AbstractContext::AbstractContext(const AGE::ContextAttributes& contextAttributes)
	{
		m_contextAttributes = contextAttributes;
	}

	AbstractContext* AbstractContext::Create(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes)
	{
		return new AbstractContextImplementation(windowHandle, contextAttributes);
	}
}