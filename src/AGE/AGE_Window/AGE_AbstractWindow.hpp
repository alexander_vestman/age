#ifndef AGE_ABSTRACT_WINDOW_HPP
#define AGE_ABSTRACT_WINDOW_HPP

//Standard incldues
#include <thread>
#include <atomic>
#include <condition_variable>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Events/AGE_Event.hpp>
#include <AGE/AGE_Window/AGE_WindowDescription.hpp>
#include <AGE/AGE_Window/AGE_ContextAttributes.hpp>

//Platform includes
#ifdef _WIN32
#	include <AGE/AGE_Window/Win32/AGE_Win32_WindowCreation.hpp>
#	include <AGE/AGE_Window/Win32/AGE_Win32_WindowEventHandler.hpp>
#	include <AGE/AGE_Window/Win32/AGE_Win32_WindowConfig.hpp>
#endif

namespace AGE
{
	class AbstractWindow
	{
	public:
		AbstractWindow();
		AbstractWindow(AGE::AbstractWindow&& rval);
		virtual ~AbstractWindow() = 0;

		static AbstractWindow* Create(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle = AGE::WindowStyle::Default);
		virtual void Close() = 0;
		virtual void Destroy() = 0;

		virtual void setEventCallback(std::function<void(const AGE::Event)> eventCallback) = 0;
		virtual void setCaption(const char* caption) = 0;
		virtual void setIcon(const AGE::Icon& icon) = 0;
		virtual void setCursor(const AGE::Cursor& cursor) = 0;
		virtual void setWindowPosition(const int x, const int y) = 0;
		virtual void setWindowSize(const int width, const int height) = 0;
		virtual void setWindowMode(const AGE::WindowMode& windowMode) = 0;

		virtual AGE::WindowHandle getWindowHandle() const = 0;

		bool isOpen() const;

	protected:	
		AGE::ContextAttributes		m_contextAttributes;
		AGE::WindowCreation			m_windowCreation;		
		AGE::WindowEventHandler*	mp_windowEventHandler;	
		AGE::WindowConfig			m_windowConfig;
		AGE::WindowHandle			m_windowHandle;		

		std::thread m_thread;
		std::atomic<bool> m_created;	
		std::condition_variable m_createCondition;
		std::mutex m_createLock;

	private:
		AbstractWindow(const AGE::AbstractWindow&);
		const AGE::AbstractWindow& operator=(const AGE::AbstractWindow&);
	};
}

#endif