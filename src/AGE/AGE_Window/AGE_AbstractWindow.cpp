#include <AGE/AGE_Window/AGE_AbstractWindow.hpp>

#if defined(AGE_SYSTEM_WINDOWS)

#include <AGE/AGE_Window/Win32/AGE_Win32_Window.hpp>
typedef AGE::Win32Window AbstractWindowImplementation;

#elif defined(AGE_SYSTEM_LINUX)


#elif defined(AGE_SYSTEM_OSX)


#endif

namespace AGE
{
	AbstractWindow::AbstractWindow()
	{	
		m_created = false;		
		mp_windowEventHandler = nullptr;		
	}

	AbstractWindow::~AbstractWindow()
	{

	}

	AbstractWindow* AbstractWindow::Create(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle)
	{
		return new AbstractWindowImplementation(contextAttributes, windowDescription, videoDescription, windowMode, windowStyle);
	}

	bool AbstractWindow::isOpen() const
	{
		return m_created;
	}
}