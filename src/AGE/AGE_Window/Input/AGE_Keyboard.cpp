#include <AGE/AGE_Window/Input/AGE_Keyboard.hpp>
#include <AGE/AGE_Window/Input/Keyboard/AGE_KeyboardImplementation.hpp>

namespace AGE
{
	AGE::KeyboardState Keyboard::getKeyboardState()
	{
		return AGE::KeyboardImplementation::getKeyboardState();
	}

	void Keyboard::setKeyState(const AGE::Key& key, const AGE::KeyInfo& keyInfo)
	{
		AGE::KeyboardImplementation::setKeyState(key, keyInfo);
	}

	void Keyboard::Capture()
	{
		AGE::KeyboardImplementation::Capture();
	}
}