#include <AGE/AGE_Window/Input/AGE_Mouse.hpp>
#include <AGE/AGE_Window/Input/Mouse/AGE_MouseImplementation.hpp>

namespace AGE
{
	AGE::MouseState Mouse::getMouseState()
	{
		return AGE::MouseImplementation::getMouseState();
	}

	void Mouse::setButtonState(const AGE::Button& button, const AGE::ButtonState& buttonState)
	{
		AGE::MouseImplementation::setButtonState(button, buttonState); 
	}

	void Mouse::addRawDelta(const long x, const long y)
	{
		AGE::MouseImplementation::addRawDelta(x, y);
	}

	void Mouse::Capture()
	{
		AGE::MouseImplementation::Capture();
	}
}