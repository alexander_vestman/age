#ifndef AGE_MOUSE_IMPLEMENTATION_HPP
#define AGE_MOUSE_IMPLEMENTATION_HPP

//Standard includes
#include <mutex>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Input/Mouse/AGE_MouseState.hpp>

namespace AGE
{
	class MouseImplementation
	{
	public:
		static AGE::MouseState getMouseState();
		static void setButtonState(const AGE::Button& button, const AGE::ButtonState& buttonState);
		static void addRawDelta(const long x, const long y);
		static void Capture();

	private:
		static AGE_ALIGN(64) AGE::MouseState m_readState;
		static AGE_ALIGN(64) AGE::MouseState m_writeState;
		static std::mutex m_copyMutex;		
	};
}

#endif