#include <AGE/AGE_Window/Input/Mouse/AGE_MouseImplementation.hpp>

namespace AGE
{
	AGE::MouseState MouseImplementation::m_readState = AGE::MouseState();
	AGE::MouseState MouseImplementation::m_writeState = AGE::MouseState();
	std::mutex MouseImplementation::m_copyMutex;

	AGE::MouseState MouseImplementation::getMouseState()
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		return m_readState;		
	}

	void MouseImplementation::setButtonState(const AGE::Button& button, const AGE::ButtonState& buttonState)
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		m_writeState.setButtonState(button, buttonState);
	}
	void MouseImplementation::addRawDelta(const long x, const long y)
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		m_writeState.m_rawDeltaX += x;
		m_writeState.m_rawDeltaY += y;
	}

	void MouseImplementation::Capture()
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		std::memcpy(&m_readState, &m_writeState, sizeof(AGE::MouseState));
		m_writeState.m_rawDeltaX = 0;
		m_writeState.m_rawDeltaY = 0;
	}
}