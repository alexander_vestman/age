#include <AGE/AGE_Window/Input/Keyboard/AGE_KeyboardImplementation.hpp>

namespace AGE
{
	AGE::KeyboardState KeyboardImplementation::m_readState = AGE::KeyboardState();
	AGE::KeyboardState KeyboardImplementation::m_writeState = AGE::KeyboardState();
	std::mutex KeyboardImplementation::m_copyMutex;

	AGE::KeyboardState KeyboardImplementation::getKeyboardState()
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		return m_readState;		
	}

	void KeyboardImplementation::setKeyState(const AGE::Key& key, const AGE::KeyInfo& keyInfo)
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		m_writeState.setKeyState(key, keyInfo);
	}

	void KeyboardImplementation::Capture()
	{
		std::lock_guard<std::mutex> LockGuard(m_copyMutex);
		std::memcpy(&m_readState, &m_writeState, sizeof(AGE::KeyboardState));
	}
}