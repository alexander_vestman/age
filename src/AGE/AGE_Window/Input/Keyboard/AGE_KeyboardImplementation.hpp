#ifndef AGE_KEYBOARD_IMPLEMENTATION_HPP
#define AGE_KEYBOARD_IMPLEMENTATION_HPP

//Standard includes
#include <mutex>

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/Input/Keyboard/AGE_KeyboardState.hpp>

namespace AGE
{
	class KeyboardImplementation
	{
	public:		
		static AGE::KeyboardState getKeyboardState();
		static void setKeyState(const AGE::Key& key, const AGE::KeyInfo& keyInfo);
		static void Capture();

	private:
		static AGE_ALIGN(64) AGE::KeyboardState m_readState;
		static AGE_ALIGN(64) AGE::KeyboardState m_writeState;
		static std::mutex m_copyMutex;
	};
}

#endif