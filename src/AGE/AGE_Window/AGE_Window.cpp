#include <AGE/AGE_Window/AGE_Window.hpp>
#include <AGE/AGE_Window/AGE_AbstractWindow.hpp>
#include <AGE/AGE_Window/AGE_AbstractContext.hpp>

namespace AGE
{
	Window::Window()
	{
		mp_abstractWindow = nullptr;
	}

	Window::Window(AGE::Window&& rval)
	{
		mp_abstractWindow = std::move(rval.mp_abstractWindow);
	}

	Window::Window(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle) 
	{
		mp_abstractWindow = nullptr;
		mp_abstractContext = nullptr;
		Create(contextAttributes, windowDescription, videoDescription, windowMode, windowStyle);
	}

	Window::~Window()
	{		
		Destroy();
	}

	void Window::SwapBuffer()
	{
		if(mp_abstractContext)		
		{
			mp_abstractContext->SwapBuffer();
		}
	}

	void Window::MakeCurrent()
	{
		if(mp_abstractContext)		
		{
			mp_abstractContext->MakeCurrent();
		}
	}	
		
	void Window::Create(AGE::ContextAttributes contextAttributes, AGE::WindowDescription windowDescription, AGE::VideoDescription videoDescription, AGE::WindowMode windowMode, AGE::WindowStyle windowStyle)
	{
		Destroy();
		mp_abstractWindow = AGE::AbstractWindow::Create(contextAttributes, windowDescription, videoDescription, windowMode, windowStyle);
		mp_abstractContext = AGE::AbstractContext::Create(mp_abstractWindow->getWindowHandle(), contextAttributes);
	}

	void Window::Destroy()
	{
		if(mp_abstractContext)		
		{
			delete mp_abstractContext;
			mp_abstractContext = nullptr;
		}

		if(mp_abstractWindow)
		{
			delete mp_abstractWindow;
			mp_abstractWindow = nullptr;
		}		
	}

	void Window::setEventCallback(std::function<void(const AGE::Event)> eventCallback)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setEventCallback(eventCallback);
		}
	}

	void Window::setCaption(const char* caption)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setCaption(caption);
		}
	}

	void Window::setIcon(const AGE::Icon& icon)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setIcon(icon);
		}
	}

	void Window::setCursor(const AGE::Cursor& cursor)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setCursor(cursor);
		}
	}

	void Window::setWindowPosition(const int x, const int y)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setWindowPosition(x, y);
		}
	}

	void Window::setWindowSize(const int width, const int height)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setWindowSize(width, height);
		}
	}

	void Window::setWindowMode(const AGE::WindowMode& windowMode)
	{
		if(mp_abstractWindow)
		{
			mp_abstractWindow->setWindowMode(windowMode);
		}
	}	

	bool Window::isOpen() const
	{		
		return mp_abstractWindow != nullptr ? mp_abstractWindow->isOpen() : false;	
	}
}