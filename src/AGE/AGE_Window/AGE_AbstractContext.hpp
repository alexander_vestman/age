#ifndef AGE_ABSTRACT_CONTEXT_HPP
#define AGE_ABSTRACT_CONTEXT_HPP

//General includes
#include <AGE/AGE_Initialize.h>
#include <AGE/AGE_Window/AGE_Window_Initialize.hpp>
#include <AGE/AGE_Window/AGE_ContextAttributes.hpp>

namespace AGE
{
	class AbstractContext
	{
	public:		
		static AbstractContext* Create(const AGE::WindowHandle windowHandle, const AGE::ContextAttributes& contextAttributes);

		virtual void SwapBuffer() = 0;
		virtual void MakeCurrent() = 0;
		const AGE::ContextAttributes& getContextAttributes() const;

	protected:
		AbstractContext(const AGE::ContextAttributes& contextAttributes);
		AGE::ContextAttributes m_contextAttributes;

	private:
		AbstractContext(const AGE::AbstractContext&);
		const AGE::AbstractContext& operator=(const AGE::AbstractContext&);
	};
}


#endif